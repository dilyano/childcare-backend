package com.dilyano.childcare.model;

public enum RoleType {
    USER,
    MODERATOR,
    ADMIN
}
