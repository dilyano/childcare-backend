package com.dilyano.childcare.model;

import org.springframework.data.annotation.Id;

public class Role {

    @Id
    private Integer id;

    private RoleType roleType;

    public Role() {

    }

    public Role(RoleType roleType) {
        this.roleType = roleType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RoleType getRoleType() {
        return roleType;
    }

    public void setRoleType(RoleType roleType) {
        this.roleType = roleType;
    }
}
