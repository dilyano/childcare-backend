package com.dilyano.childcare.repository;

import com.dilyano.childcare.model.Role;
import com.dilyano.childcare.model.RoleType;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends MongoRepository<Role, String> {
    Optional<Role> findByRoleType(RoleType roleType);
}
